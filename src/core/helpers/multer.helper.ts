import {Request} from 'express';
import {join} from 'path';
import {v4 as uuid} from 'uuid';

export class MulterHelper {

    public static bannerDestination(
        request: Request,
        file: Express.Multer.File,
        callback: (error: Error | null, destination: string) => void
    ): void {
        callback(null, join(__dirname, '../../../../public/banner/'));
    }

    public static destination(
        request: Request,
        file: Express.Multer.File,
        callback: (error: Error | null, destination: string) => void
    ): void {
        callback(null, join(__dirname, '../../../../public/upload/'));
    }

    public static filenameHandler(
        request: Request,
        file: Express.Multer.File,
        callback: (error: Error | null, destination: string) => void
    ): void {
        const originalName = file.originalname;
        const fileType = originalName.split(".").pop();
        const id: string = uuid();
        callback(null, `${id}.${fileType}`);
    }

}