import {Body, Controller, Get, Post, Query, UploadedFile, UseInterceptors} from '@nestjs/common';
import {FileInterceptor} from "@nestjs/platform-express";
import {ApiService} from "./api.service";
import {formatInTimeZone, utcToZonedTime, zonedTimeToUtc} from "date-fns-tz";
import {format, parse, parseISO} from "date-fns";

@Controller('api')
export class ApiController {
    constructor(private readonly apiService: ApiService) {
    }

    @Get('test')
    test() {
        return this.apiService.test();
    }

    @Get('findNoticeByLimit')
    findNoticeByLimit(@Query() query: string) {
        return this.apiService.findNoticeByLimit(query);
    }

    @Post('receiveMaskData')
    @UseInterceptors(FileInterceptor('file'))
    receivePredictionData(@Body() maskJson: String, @UploadedFile() file: Express.Multer.File) {
        this.apiService.receiveMaskData(maskJson, file);
    }

    @Get('appGetBanner')
    appGetBanner() {
        return this.apiService.appGetBanner();
    }

}
