import {Injectable} from '@nestjs/common';
import {PrismaService} from "../../prisma.service";
import {notice, Prisma} from '@prisma/client'
import {format, getTime, parse, parseISO} from "date-fns";
import {formatInTimeZone, toDate} from 'date-fns-tz'
import {map} from "rxjs";
import {HttpService} from "@nestjs/axios";

@Injectable()
export class ApiService {
    constructor(private prismaService: PrismaService, private readonly http: HttpService) {
    }

    test() {
        const url = "https://fcm.googleapis.com/fcm/send";
        const config = {
            headers: {
                Authorization: `key=${process.env.FIREBASE_SERVER_KEY}`,
                "Content-Type": "application/json"
            }
        };
        return this.http.post(
            url,
            {
                "data": {
                    id: 34,
                    image: "https://i.imgur.com/h8Ha0Yd.png",
                },
                "notification": {
                    "click_action": "FLUTTER_NOTIFICATION_CLICK",
                    "title": 'test',
                    "body": "test send message<br>test send message2<br>test send message3<br>test send message4",
                    "image": "https://i.imgur.com/h8Ha0Yd.png",
                },
                "to": process.env.REGISTRATION_TOKEN
            },
            config
        ).subscribe(
            (res) => {
                console.log(res.data);
            },
            (error) => {
                console.log(error);
            }
        );
    }

    sendFcmToPhone(notice) {
        let body = "";
        body += "With mask: [" + notice.with_mask_count.toString() + "]";
        body += "Mask weared incorrect: [" + notice.mask_weared_incorrect_count.toString() + "]";
        body += "Without mask: [" + notice.without_mask_count.toString() + "]";
        body += "Total: [" + notice.total_count.toString() + "]";

        const url = "https://fcm.googleapis.com/fcm/send";
        const config = {
            headers: {
                Authorization: `key=${process.env.FIREBASE_SERVER_KEY}`,
                "Content-Type": "application/json"
            }
        };
        this.http.post(
            url,
            {
                "data": {
                    id: notice.id,
                    image: "https://i.imgur.com/h8Ha0Yd.png",
                },
                "notification": {
                    "click_action": "FLUTTER_NOTIFICATION_CLICK",
                    "title": notice.location.name,
                    "body": body,
                    "image": "https://i.imgur.com/h8Ha0Yd.png",
                },
                "to": process.env.REGISTRATION_TOKEN
            },
            config
        ).subscribe(
            (res) => {
                console.log(res.data);
            },
            (error) => {
                console.log(error);
            }
        );
    }

    async receiveMaskData(maskJson, image) {
        const mediaJson = {
            "url": process.env.HOST_URL + "public/upload/" + image.filename,
            "path": "public/upload/" + image.filename,
            "fileType": image.filename.split(".").pop(),
        }
        let mask_weared_incorrect: number = 0;
        let with_mask: number = 0;
        let without_mask: number = 0;
        let total: number = 0;
        Object.entries(maskJson).forEach(
            ([key, value]) => {
                switch (key) {
                    case "mask_weared_incorrect": {
                        mask_weared_incorrect = parseInt(<string>value);
                        break;
                    }
                    case "with_mask": {
                        with_mask = parseInt(<string>value);
                        break;
                    }
                    case "without_mask": {
                        without_mask = parseInt(<string>value);
                        break;
                    }
                    default: {
                        break;
                    }
                }
                total += parseInt(<string>value);
            }
        );
        const notice = await this.prismaService.notice.create({
            "data": {
                location_id: Math.floor(Math.random() * 10) + 1,
                mask_weared_incorrect_count: mask_weared_incorrect,
                with_mask_count: with_mask,
                without_mask_count: without_mask,
                total_count: total,
                media: JSON.stringify(mediaJson),
                raw_json: JSON.stringify(maskJson),
                created_at: new Date()
            },
            include: {
                location: true,
            },
        });
        if (process.env.APP_DEBUG != "true") {
            this.sendFcmToPhone(notice);
        }
    }

    async findNoticeByLimit(query) {
        if (query.limit && query.page) {
            return await this.prismaService.notice.findMany({
                skip: parseInt(query.limit) * ((parseInt(query.page) <= 0 ? 1 : parseInt(query.page)) - 1),
                take: parseInt(query.limit),
                orderBy: {
                    created_at: 'desc',
                },
                include: {
                    location: true,
                },
            });
        }
        return await this.prismaService.notice.findMany({
            orderBy: {
                created_at: 'desc',
            },
            include: {
                location: true,
            },
        });
    }

    async appGetBanner() {
        const banner = await this.prismaService.banner.findMany({
            where: {
                status: 1,
            },
            select: {
                id: true,
                media: true,
            },
            orderBy: {
                created_at: 'desc',
            },
        });
        const formatedBannerList = [];
        banner.forEach(function (item) {
            const tempObject = {
                id: item.id,
                media: JSON.parse(item.media).path,
            }
            formatedBannerList.push(tempObject);
        });
        return formatedBannerList;
    }

}
