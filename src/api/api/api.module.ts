import {Module} from '@nestjs/common';
import {ApiService} from './api.service';
import {ApiController} from './api.controller';
import {PrismaService} from "../../prisma.service";
import {MulterModule} from "@nestjs/platform-express";
import {diskStorage} from "multer";
import {MulterHelper} from "../../core/helpers/multer.helper";
import {HttpModule} from "@nestjs/axios";

@Module({
    imports: [
        MulterModule.register({
            storage: diskStorage({
                destination: MulterHelper.destination,
                filename: MulterHelper.filenameHandler
            })
        }),
        HttpModule
    ],
    controllers: [ApiController],
    providers: [ApiService, PrismaService]
})
export class ApiModule {
}
