import {applyDecorators, Controller} from '@nestjs/common';

export function AdminRoute(route: String) {
    return applyDecorators(
        Controller("admin/api/" + route)
    );
}
