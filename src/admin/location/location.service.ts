import {Injectable} from '@nestjs/common';
import {PrismaService} from "../../prisma.service";
import {Prisma} from '@prisma/client';

@Injectable()
export class LocationService {
    constructor(private prismaService: PrismaService) {
    }

    async create(data: Prisma.locationCreateInput) {
        data.created_at = new Date();
        const location = await this.prismaService.location.create({
            "data": {
                name: data.name,
                created_at: new Date()
            },
        });
        return {
            'success': true,
            'id': location.id,
            'message': "Success to create location",
        };
    }

    async findAll() {
        return this.prismaService.location.findMany({
            where: {
                deleted_at: null
            },
            select: {
                id: true,
                name: true,
                created_at: true,
                updated_at: true,
            },
            orderBy: {
                created_at: 'desc',
            },
        });
    }

    async findAllTrashed() {
        return this.prismaService.location.findMany({
            where: {
                NOT: {
                    deleted_at: null
                },
            },
            select: {
                id: true,
                name: true,
                created_at: true,
                updated_at: true,
            },
            orderBy: {
                deleted_at: 'desc',
            },
        });
    }

    async findOne(id: number) {
        return await this.prismaService.location.findUnique({
            where: {
                id: id,
            },
            select: {
                id: true,
                name: true,
                created_at: true,
                updated_at: true,
            },
        });
    }

    async update(id: number, data: Prisma.locationUpdateInput) {
        data.updated_at = new Date();
        const location = await this.prismaService.location.update({
            where: {
                id: id
            },
            data,
        });
        return {
            'success': true,
            'id': location.id,
            'message': "Success to update location",
        };
    }

    async remove(id: number) {
        await this.prismaService.location.update({
            where: {
                id: id,
            },
            data: {
                deleted_at: new Date(),
            },
        });
        return {
            'success': true,
            'message': "Success to delete location",
        };
    }

    async restore(id: number) {
        await this.prismaService.location.update({
            where: {
                id: id,
            },
            data: {
                deleted_at: null,
            },
        });
        return {
            'success': true,
            'message': "Success to restore location",
        };
    }
}
