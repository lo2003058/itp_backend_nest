import {Module} from '@nestjs/common';
import {NoticeService} from './notice.service';
import {NoticeController} from './notice.controller';
import {PrismaService} from "../../prisma.service";

@Module({
    controllers: [NoticeController],
    providers: [NoticeService, PrismaService]
})
export class NoticeModule {
}
