import {Injectable} from '@nestjs/common';
import {PrismaService} from "../../prisma.service";
import {format, parseISO} from "date-fns";
import {Workbook} from "exceljs";
import {v4 as uuid} from 'uuid';
import {join} from "path";
import {_} from "lodash";

@Injectable()
export class NoticeService {
    constructor(private prismaService: PrismaService) {
    }

    async getOptions() {
        const location = await this.prismaService.location.findMany({
            where: {
                deleted_at: null
            },
            select: {
                id: true,
                name: true,
            },
            orderBy: {
                name: 'asc',
            },
        });
        return {
            location: location,
        }
    }

    async findAll(query) {
        if (query.filter) {
            const filter_col = ['location_id', 'date'];
            const filter = JSON.parse(query.filter);
            const filterObject = {};
            for (const [key, value] of Object.entries(filter)) {
                if (_.includes(filter_col, key)) {
                    if (typeof value == "number" && value) {
                        filterObject['location_id'] = value;
                    } else if (typeof value == "object" && key == "date" && !_.isEmpty(value)) {
                        filterObject['created_at'] = {
                            gte: parseISO(value[0]),
                            lt: parseISO(value[1]),
                        };
                    }
                }
            }
            if (!_.isEmpty(filterObject)) {
                return await this.prismaService.notice.findMany({
                    where: filterObject,
                    orderBy: {
                        created_at: 'desc',
                    },
                    include: {
                        location: true,
                    },
                });
            }
        }
        return await this.prismaService.notice.findMany({
            orderBy: {
                created_at: 'desc',
            },
            include: {
                location: true,
            },
        });
    }

    async findOne(id: number) {
        return await this.prismaService.notice.findUnique({
            where: {
                id: id,
            },
            include: {
                location: true,
            },
        });
    }

    async findNoticeRecordByDate(params: any) {
        return await this.prismaService.notice.findMany({
            where: {
                created_at: {
                    gte: params.start_date,
                    lt: params.end_date,
                },
            },
            select: {
                id: true,
                mask_weared_incorrect_count: true,
                with_mask_count: true,
                without_mask_count: true,
                total_count: true,
                created_at: true,
            },
            orderBy: {
                created_at: 'asc',
            },
        });
    }

    async exportData(params: any) {
        let book = new Workbook();
        let sheet = book.addWorksheet('Record');
        let header = [
            'id', 'location', 'With mask', 'Mask weared incorrect',
            'Without mask', 'Total', 'Created at'
        ];
        sheet.addRow(header);
        console.log(params);
        const dataArr = await this.prismaService.notice.findMany({
            where: {
                created_at: {
                    gte: params.start_date,
                    lt: params.end_date,
                },
            },
            include: {
                location: true,
            },
        });
        dataArr.forEach(function (data) {
            sheet.addRow([
                data.id, data.location.name, data.with_mask_count, data.mask_weared_incorrect_count,
                data.without_mask_count, data.total_count, format(data.created_at, 'yyyy-MM-dd HH:mm:ss')
            ]);
        });
        let fileName = "record_" + uuid() + ".csv";
        let filePathAndName = join(__dirname, '../../../../public/csv/') + fileName;
        await book.csv.writeFile(filePathAndName);
        return {
            filePathAndName: filePathAndName,
            fileName: fileName
        };
    }


}
