import {Get, Post, Body, Param, Res, StreamableFile, Response, Query} from '@nestjs/common';
import {NoticeService} from './notice.service';
import {AdminRoute} from "../../common/adminRoute.decorator";
import {createReadStream} from "fs";

@AdminRoute('notice')
export class NoticeController {
    constructor(private readonly noticeService: NoticeService) {
    }

    // @Post()
    // create(@Body() createNoticeDto: CreateNoticeDto) {
    //     return this.noticeService.create(createNoticeDto);
    // }

    @Get("options")
    getOptions() {
        return this.noticeService.getOptions();
    }

    @Get()
    findAll(@Query() query: string) {
        return this.noticeService.findAll(query);
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.noticeService.findOne(+id);
    }

    @Post('findNoticeRecordByDate')
    findNoticeRecordByDate(@Body() data) {
        return this.noticeService.findNoticeRecordByDate(data);
    }

    @Post('exportData')
    async exportData(@Body() data, @Response({passthrough: true}) res): Promise<StreamableFile> {
        const fileData = await this.noticeService.exportData(data);
        res.header('X-File-Name', fileData.fileName);
        res.header(
            'Content-Disposition',
            'attachment; filename=' + fileData.fileName,
        );
        return new StreamableFile(createReadStream(fileData.filePathAndName));
    }

}
