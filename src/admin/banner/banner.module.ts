import {Module} from '@nestjs/common';
import {BannerService} from './banner.service';
import {BannerController} from './banner.controller';
import {PrismaService} from "../../prisma.service";
import {MulterModule} from "@nestjs/platform-express";
import {diskStorage} from "multer";
import {MulterHelper} from "../../core/helpers/multer.helper";

@Module({
    imports: [
        MulterModule.register({
            storage: diskStorage({
                destination: MulterHelper.bannerDestination,
                filename: MulterHelper.filenameHandler
            })
        })
    ],
    controllers: [BannerController],
    providers: [BannerService, PrismaService]
})
export class BannerModule {
}
