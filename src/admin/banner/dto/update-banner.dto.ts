import { PartialType } from '@nestjs/mapped-types';
import { CreateBannerDto } from './create-banner.dto';

export class UpdateBannerDto extends PartialType(CreateBannerDto) {
    public description: string;

    public media?: string;

    public status: number;

    public created_at: Date;

    public updated_at: Date;
}
