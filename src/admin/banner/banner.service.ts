import {Injectable} from '@nestjs/common';
import {PrismaService} from "../../prisma.service";
import {Prisma} from '@prisma/client';
import {join} from 'path';
import * as fs from 'fs';

@Injectable()
export class BannerService {
    constructor(private prismaService: PrismaService) {
    }

    findAll() {
        return this.prismaService.banner.findMany({
            select: {
                id: true,
                description: true,
                media: true,
                status: true,
                created_at: true,
                updated_at: true,
            },
            orderBy: {
                created_at: 'desc',
            },
        });
    }

    async findOne(id: number) {
        return await this.prismaService.banner.findUnique({
            where: {
                id: id,
            },
            select: {
                id: true,
                description: true,
                media: true,
                status: true,
                created_at: true,
                updated_at: true,
            },
        });
    }

    async create(data: Prisma.bannerCreateInput, file) {
        const mediaJson = {
            "url": process.env.HOST_URL + "public/banner/" + file.filename,
            "path": "public/banner/" + file.filename,
            "fileName": file.filename,
            "fileType": file.filename.split(".").pop(),
        }
        const banner = await this.prismaService.banner.create({
            "data": {
                description: data.description,
                media: JSON.stringify(mediaJson),
                status: data.status,
                created_at: new Date()
            },
        });
        return {
            'success': true,
            'id': banner.id,
            'message': "Success to create banner",
        };
    }

    async update(id: number, data: Prisma.bannerUpdateInput, file) {
        if (file) {
            const mediaJson = {
                "url": process.env.HOST_URL + "public/banner/" + file.filename,
                "path": "public/banner/" + file.filename,
                "fileName": file.filename,
                "fileType": file.filename.split(".").pop(),
            }
            data = {
                description: data.description,
                media: JSON.stringify(mediaJson),
                status: data.status,
                updated_at: new Date()
            };
        } else {
            data = {
                description: data.description,
                status: data.status,
                updated_at: new Date()
            };
        }
        const banner = await this.prismaService.banner.update({
            where: {
                id: id
            },
            "data": data,
        });
        return {
            'success': true,
            'id': banner.id,
            'message': "Success to update banner",
        };
    }

    removeBannerInSystem(fileName) {
        fs.unlink(join(__dirname, '../../../../public/banner/') + fileName, (err) => {
            if (err) throw err;
            console.log(fileName + ' was deleted');
        });
    }

    async remove(id: number) {
        const banner = await this.prismaService.banner.delete({
            where: {
                id: id,
            },
        });
        this.removeBannerInSystem(JSON.parse(banner.media).fileName);
        return {
            'success': true,
            'message': "Success to delete banner",
        };
    }
}
