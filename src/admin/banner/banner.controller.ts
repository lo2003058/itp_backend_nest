import {Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFile} from '@nestjs/common';
import {BannerService} from './banner.service';
import {CreateBannerDto} from './dto/create-banner.dto';
import {UpdateBannerDto} from './dto/update-banner.dto';
import {AdminRoute} from "../../common/adminRoute.decorator";
import {FileInterceptor} from "@nestjs/platform-express";

@AdminRoute('banner')
export class BannerController {
    constructor(private readonly bannerService: BannerService) {
    }

    @Post()
    @UseInterceptors(FileInterceptor('file'))
    create(@Body() data, @UploadedFile() file: Express.Multer.File) {
        const dataObject = JSON.parse(data.data);
        return this.bannerService.create(dataObject, file);
    }

    @Get()
    findAll() {
        return this.bannerService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.bannerService.findOne(+id);
    }

    @Patch(':id')
    @UseInterceptors(FileInterceptor('file'))
    update(@Param('id') id: string, @Body() data, @UploadedFile() file: Express.Multer.File) {
        if (file) {
            const dataObject = JSON.parse(data.data);
            return this.bannerService.update(+id, dataObject, file);
        }
        return this.bannerService.update(+id, data, file);
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.bannerService.remove(+id);
    }
}
