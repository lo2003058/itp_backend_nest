import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {LocationModule} from './admin/location/location.module';
import {NoticeModule} from './admin/notice/notice.module';
import {ApiModule} from './api/api/api.module';
import {PrismaService} from './prisma.service';
import {ConfigModule} from '@nestjs/config';
import {join} from 'path';
import { BannerModule } from './admin/banner/banner.module';

@Module(
    {
        imports: [
            ConfigModule.forRoot({
                isGlobal: true,
            }),
            LocationModule,
            NoticeModule,
            ApiModule,
            BannerModule,
        ],
        controllers: [AppController],
        providers: [
            AppService,
        ],
    }
)
export class AppModule {
}
