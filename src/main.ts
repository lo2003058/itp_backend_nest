import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {PrismaService} from "./prisma.service";
import {join} from "path";
import {NestExpressApplication} from "@nestjs/platform-express";

async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule);
    app.enableCors({
        exposedHeaders: ['X-File-Name'],
    });

    //don't edit
    // app.useStaticAssets(join(__dirname, '../..', 'public/upload'), {
    //     index: false,
    //     prefix: '/public/upload/',
    // });

    app.useStaticAssets(join(__dirname, '../..', 'public'), {
        index: false,
        prefix: '/public/',
    });
    await app.listen(8000);
    const prismaService = app.get(PrismaService);
    await prismaService.enableShutdownHooks(app)
}

bootstrap();
