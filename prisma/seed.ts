import {PrismaClient} from '@prisma/client'

const prisma = new PrismaClient()

async function main() {
    for (let i = 1; i < 11; i++) {
        await prisma.location.create({
            data: {
                name: 'location' + i,
                created_at: new Date(),
            },
        })
    }
}

main()
    .catch((e) => {
        console.error(e)
        process.exit(1)
    })
    .finally(async () => {
        await prisma.$disconnect()
    })